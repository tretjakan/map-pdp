import React from 'react'
import { Text, StyleSheet } from 'react-native';

const Typography = (props) => (
  <Text style={styles.mainText}>{props.children}</Text>
);

const styles = StyleSheet.create({
  mainText: {
    color: '#182f38',
    backgroundColor: 'transparent',
  },
});

export { Typography };
