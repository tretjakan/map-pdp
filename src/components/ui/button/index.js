import React from 'react'
import {
  TouchableOpacity, TouchableNativeFeedback,
  Text, View, StyleSheet, Platform,
} from 'react-native';

export const Button = ({ onPress, children, color, isDisabled }) => {
  const content = (
    <View style={[styles.button, { backgroundColor: color || '#e4e7eb' }, isDisabled ? styles.disabled : null]}>
      <Text style={isDisabled ? styles.disabledText : null}>{children}</Text>
    </View>
  );
  if (isDisabled) {
    return content;
  }
  if (Platform.OS === 'android') {
    return (
      <TouchableNativeFeedback onPress={onPress}>
        {content}
      </TouchableNativeFeedback>
    );
  }
  return (
    <TouchableOpacity onPress={onPress}>
      {content}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    padding: 10,
    margin: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#182f38',
  },
  disabled: {
    backgroundColor: '#eee',
    borderColor: '#aaa',
  },
  disabledText: {
    color: '#aaa',
  },
});