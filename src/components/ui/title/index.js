import React from 'react'
import { Text, StyleSheet } from 'react-native';

export const Title = props => (
    <Text
        {...props}
        style={[styles.title, props.style]}
    >
        {props.children}
    </Text>
)

const styles = StyleSheet.create({
    title: {
        fontSize: 28,
        fontWeight: 'bold',
        color: '#fff',
        backgroundColor: 'transparent',
    },
});
