import React from 'react'
import { TextInput, StyleSheet } from 'react-native';

export const DefaultInput = props => (
    <TextInput
        underlineColorAndroid="transparent"
        {...props}
        style={
            [styles.input, props.style, !props.isValid && props.isTouched && styles.error]
        }
    />
);

const styles = StyleSheet.create({
    input: {
        width: '100%',
        borderWidth: 1,
        padding: 5,
        marginTop: 8,
        marginBottom: 8,
        backgroundColor: '#e4e7eb',
        borderColor: '#bbb',
    },
    error: {
        backgroundColor: '#f9c0c0',
        borderColor: 'red',
    },
})
