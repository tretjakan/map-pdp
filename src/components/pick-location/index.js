import React, { Component } from 'react'
import { View, Text, Button, StyleSheet, Dimensions } from 'react-native';
import MapView from 'react-native-maps';

class PickLocation extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount = () => {
    this.onReset();
  }

  onReset = () => {
    this.setState({
      focusedRegion: {
        latitude: 37.7900352,
        longitude: -122.4013726,
        latitudeDelta: 0.0122,
        longitudeDelta: Dimensions.get('window').width / Dimensions.get('window').height * 0.0122,
      },
      regionChosen: false,
    });
  }

  onPickLocation = (event) => {
    const { focusedRegion } = this.state;
    const { onLocationPick } = this.props;
    const coords = event.nativeEvent.coordinate;
    this.map.animateToRegion({
      ...focusedRegion,
      latitude: coords.latitude,
      longitude: coords.longitude,
    });
    this.setState((prevState) => {
      return {
        focusedRegion: {
          ...prevState.focusedRegion,
          latitude: coords.latitude,
          longitude: coords.longitude,
        },
        regionChosen: true,
      }
    });
    onLocationPick({
      latitude: coords.latitude,
      longitude: coords.longitude,
    });
  }

  onGetLocation = () => {
    navigator.geolocation.getCurrentPosition(({ coords }) => {
      const coordsEvent = {
        nativeEvent: {
          coordinate: {
            latitude: coords.latitude,
            longitude: coords.longitude,
          },
        },
      };
      this.onPickLocation(coordsEvent);
    }, err => console.error(err));
  }

  render() {
    let marker = null;
    const { focusedRegion, regionChosen } = this.state;

    if (regionChosen) {
      marker = <MapView.Marker coordinate={focusedRegion} />
    }

    return (
      <View style={styles.container}>
        <MapView
          initialRegion={focusedRegion}
          // region={!regionChosen ? focusedRegion : {}}
          style={styles.map}
          onPress={(event) => this.onPickLocation(event)}
          ref={ref => this.map = ref}
        >
          {marker}
        </MapView>
        <View style={styles.button}>
          <Button title="Locate Me" onPress={this.onGetLocation}/>
        </View>
      </View>
    )
  }
}

export { PickLocation };

const styles = StyleSheet.create({
  map: {
    width: '100%',
    height: 250,
  },
  button: {
    margin: 8,
  },
  container: {
    width: '100%',
    alignItems: 'center',
  },
})