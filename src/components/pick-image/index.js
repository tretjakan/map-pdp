import React, { Component } from 'react'
import { View, Image, Button, StyleSheet } from 'react-native';
import ImagePicker from 'react-native-image-picker';

class PickImage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      pickedImage: null,
    }
  }


  onImagePick = () => {
    const { onImagePicked } = this.props;
    ImagePicker.showImagePicker({
      title: 'Pick an Image',
      maxHeight: 600,
      maxWidth: 800,
    }, res => {
      if (res.didCancel) {
        console.log('user cancelled');
      } else if (res.error) {
        console.log('error : ', res.error);
      } else {
        this.setState({
          pickedImage: { uri: res.uri },
        });
        onImagePicked({ uri: res.uri, base64: res.data });
      }
    });
  }

  onReset = () => {
    this.setState({
      pickedImage: null,
    })
  }

  render() {
    const { pickedImage } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.placeholder}>
          <Image source={pickedImage} style={styles.image} />
        </View>
        <View style={styles.button}>
          <Button title="Pick Image" onPress={this.onImagePick} />
        </View>
      </View>
    )
  }
}

export { PickImage };

const styles = StyleSheet.create({
  placeholder: {
    borderWidth: 1,
    borderColor: '#182f38',
    backgroundColor: '#e4e7eb',
    width: '80%',
    height: 150,
  },
  button: {
    margin: 8,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  container: {
    width: '100%',
    alignItems: 'center',
  },
})