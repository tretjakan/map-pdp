import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import reducer from './reducers/places';
import { loadingReducer } from './reducers/ui';
import { authReducer } from './reducers/auth';

const root = combineReducers({
    places: reducer,
    ui: loadingReducer,
    auth: authReducer,
});

let composeEnhancers = compose;

if (__DEV__) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const configureStore = () => {
    return createStore(root, composeEnhancers(applyMiddleware(thunk)));
}

export { configureStore };

