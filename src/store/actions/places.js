import * as actions from './actionTypes';
import { startLoading, stopLoading, onGetToken } from './index';

export const startAddPlace = () => {
  return {
    type: actions.START_ADD_PLACE,
  }
};

export const addPlace = (name, location, image) => (dispatch) => {
  let authToken = null;
  dispatch(onGetToken())
    .then((token) => {
      dispatch(startLoading());
      authToken = token;
      return fetch('https://us-central1-map-app-1554061408740.cloudfunctions.net/storeImage', {
        method: 'POST',
        body: JSON.stringify({
          image: image.base64,
        }),
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .catch(error => {
          console.log(error);
          alert('Something went wrong.');
          dispatch(stopLoading());
        })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw(new Error());
          }
        })
        .then((parsedRes) => {
          const data = {
            name,
            location,
            image: parsedRes.imageUrl,
            imagePath: parsedRes.imagePath, 
          };

          return fetch(`https://map-app-1554061408740.firebaseio.com/places.json?auth=${authToken}`, {
            method: 'POST',
            body: JSON.stringify(data),
          })
        })

        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw(new Error());
          }
        })
        .then((_parsedRes) => {
          dispatch(stopLoading());
          dispatch(placeAdded());
        })
        .catch(error => {
          console.log(error);
          alert('Something went wrong.');
          dispatch(stopLoading());
        })
    })
    .catch(() => alert('No Token found'));
};

export const placeAdded = () => {
  return {
    type: actions.PLACE_ADDED,
  };
}

export const getPlaces = () => {
  return (dispatch) => {
    dispatch(onGetToken())
      .then((token) => {
        return fetch(`https://map-app-1554061408740.firebaseio.com/places.json?auth=${token}`)
          .then(res => {
            if (res.ok) {
              return res.json();
            } else {
              throw(new Error());
            }
          })
          .then((parsedRes) => {
            const places = [];
            for (let key in parsedRes) {
              places.push({
                ...parsedRes[key],
                image: {
                  uri: parsedRes[key].image,
                },
                key,
              });
            }
            dispatch(setPlaces(places))
          })
          .catch((error) => {
            alert('Something went wrong with upload.')
            console.log(error);
          })
      })
      .catch(() => alert('No Token found'));
  }
}

export const setPlaces = (places) => {
  return {
    type: actions.SET_PLACES,
    payload: places,
  };
};

export const deletePlace = (key) => {
  return dispatch => {
    dispatch(onGetToken())
      .then((token) => {
        return fetch(`https://map-app-1554061408740.firebaseio.com/places/${key}.json?auth=${token}`, {
          method: "DELETE"
        })
          .then(res => {
            if (res.ok) {
              return res.json();
            } else {
              throw(new Error());
            }
          })
          .then(parsedRes => {
            alert('Done!');
            console.log("Done!");
            dispatch(removePlace(key));
          })
          .catch(err => {
            alert("Something went wrong, sorry :/");
            console.log(err);
          });
      })
      .catch(() => alert('No Token found'));


  };
};

export const removePlace = (key) => {
  return {
    type: actions.REMOVE_PLACE,
    key: key
  };
};
