import { AsyncStorage } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { AUTH_SET_TOKEN, LOG_OUT } from './actionTypes';
import { startLoading, stopLoading } from './ui';
import { mainPage, rootTemplate } from '@templates';
const KEY = 'AIzaSyDBEVkVU67SCJDM-FVGRNmK7TUDaFh5kQI';

export const tryAuth = (authData, mode) => dispatch => {
  dispatch(startLoading());
  const url = mode === 1 ? `https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=${KEY}` : `https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=${KEY}`
  if (mode === 'login') {
    dispatch(onAuth(authData, url));
  } else {
    dispatch(onAuth(authData, url));
  }
};

export const onAuth = ({ email, password }, url) => (dispatch) => {
  fetch(url, {
    method: 'POST',
    body: JSON.stringify({
      email, password, returnSecureToken: true,
    }),
    headers: {
      'Content-Type': 'application/json',
    }
  })
    .catch(error => {
      console.error(error);
      alert('Auth failed');
      dispatch(stopLoading());
    })
    .then(res => {
      if (res.ok) {
        return res.json();
      } else {
        throw(new Error());
      }
    })
    .then(parsed => {
      dispatch(stopLoading());
      if (!parsed.idToken) {
        console.log(parsed.error);
        alert(parsed.error.message);
      } else {
        dispatch(authStoreToken(parsed.idToken, parsed.expiresIn, parsed.refreshToken));
        mainPage();
      }

    });
};

export const setToken = (token, expirationDate) => ({
  type: AUTH_SET_TOKEN,
  token,
  expirationDate,
});

export const authStoreToken = (token, expiresIn, refreshToken) => {
  return dispatch => {
    const now = new Date();
    const expirationDate = now.getTime() + expiresIn * 1000;
    dispatch(setToken(token, expirationDate));
    console.log(now, new Date(expirationDate));
    AsyncStorage.setItem('token', token);
    AsyncStorage.setItem('expirationDate', expirationDate.toString());
    AsyncStorage.setItem('refreshToken', refreshToken);
  }
}

export const onGetToken = () => {
  return (dispatch, getState) => {
    const promise = new Promise((resolve, reject) => {
      const token = getState().auth.token;
      const expirationDate = getState().auth.expirationDate;
      if (!token || new Date(expirationDate) <= new Date()) {
        let fetchedToken = null;
        AsyncStorage.getItem('token')
          .catch(() => reject())
          .then(tokenFromStore => {
            fetchedToken = tokenFromStore;
            if (!tokenFromStore) {
              reject();
              return;
            }
            return AsyncStorage.getItem('expirationDate');
          })
          .then(expiryDate => {
            const parsedExpiryDate = new Date(parseInt(expiryDate, 10));
            const now = new Date();
            if (parsedExpiryDate > now) {
              dispatch(setToken(fetchedToken, parsedExpiryDate));
              resolve(fetchedToken);
            } else {
              reject();
            }
          })
          .catch(err => reject());
      } else {
        resolve(token);
      }
    });
    return promise
      .catch(() => {
        return AsyncStorage.getItem('refreshToken')
          .then(refreshToken => {
            return fetch(`https://securetoken.googleapis.com/v1/token?key=${KEY}`, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              body: `grant_type=refresh_token&refresh_token=${refreshToken}`,
            })
          })
          .then(res => {
            if (res.ok) {
              return res.json();
            } else {
              throw(new Error());
            }
          })
          .then((parsed) => {
            if (parsed.id_token) {
              dispatch(authStoreToken(parsed.id_token, parsed.expires_in, parsed.refresh_token));
              return parsed.id_token;
            } else {
              dispatch(authClearStorage());
            }
          });
      })
      .then((token) => {
        if (!token) {
          throw (new Error());
        }
        return token;
      });
  }
};

export const authAutoSingIn = () => {
  return dispatch => {
    dispatch(onGetToken())
      .then((token) => {
        mainPage();
      })
      .catch((error) => console.log('Failed to fetch', error));
  }
}

export const authClearStorage = () => {
  return (dispatch) => {
    AsyncStorage.removeItem('token');
    AsyncStorage.removeItem('expirationDate');
    return AsyncStorage.removeItem('refreshToken');
  }
}

export const logout = () => {
  return dispatch => {
    dispatch(authClearStorage())
      .then(() => {
        Navigation.setRoot(rootTemplate);
      })
    dispatch(authRemoveToken());
  }
};

export const authRemoveToken = () => {
  return {
    type: LOG_OUT,
  }
};