import * as actions from '../actions/actionTypes';

const initialState = {
  places: [],
  isPlaceAdded: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_PLACES: {
      return {
        ...state,
        places: action.payload,
      }
    }
    case actions.REMOVE_PLACE:
      return {
        ...state,
        places: state.places.filter(place => {
          return place.key !== action.key;
        })
      };
    case actions.PLACE_ADDED: {
      return {
        ...state,
        isPlaceAdded: true,
      }
    }
    case actions.START_ADD_PLACE: {
      return {
        ...state,
        isPlaceAdded: false,
      }
    }
    default: return state;
  }
};

export default reducer;