import { AUTH_SET_TOKEN, LOG_OUT } from '../actions/actionTypes';

const initialState = {
  token: null,
  expirationDate: null,
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_SET_TOKEN: {
      return {
        ...state,
        token: action.token,
        expirationDate: action.expirationDate,
      }
    }
    case LOG_OUT: {
      return {
        token: null,
        expirationDate: null,
      };
    }
    default: {
      return state
    };
  }
};

export { authReducer };