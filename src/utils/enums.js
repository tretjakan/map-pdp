const ScreenNames = {
  authScreen: 'map.AuthScreen',
  sideDrawer: 'map.SideDrawerScreen',
  findPlace: 'map.FindPlaceScreen',
  sharePlace: 'map.SharePlaceScreen',
  placeDetail: 'map.PlaceDetailScreen',
};

const ScreenIds = {
  main: 'MapApp',
  placeDetail: 'PlaceDetail',
  sideDrawerToggle: 'sideDrawerToggle',
}

const ViewModes = {
  portrait: 'portrait',
  landscape: 'landscape',
};

export { ScreenNames, ScreenIds, ViewModes };
