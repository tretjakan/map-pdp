import Icon from 'react-native-vector-icons/Ionicons';
import {
  Platform,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { ScreenNames, ScreenIds } from '@utils/enums';

const mainPage = () => {
  Promise.all([
    Icon.getImageSource(Platform.OS === 'android' ? 'md-map' : 'ios-map', 30),
    Icon.getImageSource(Platform.OS === 'android' ? 'md-share-alt' : 'ios-share-alt', 30),
    Icon.getImageSource(Platform.OS === 'android' ? 'md-menu' : 'ios-menu', 30),
  ])
    .then(sources => {
      return Navigation.setRoot({
        root: {
          sideMenu: {
            left: {
              component: {
                name: ScreenNames.sideDrawer,
              },
              options: {
                visible: false,
              }
            },
            center: {
              bottomTabs: {
                children: [
                  {
                    stack: {
                      children: [
                        {
                          component: {
                            id: ScreenIds.placeDetail,
                            name: ScreenNames.findPlace,
                            options: {
                              bottomTab: {
                                text: 'Find Place',
                                icon: sources[0],
                                selectedIconColor: '#3f6a81',
                                iconColor: '#8299b4',
                              },
                              topBar: {
                                leftButtons: {
                                  icon: sources[2],
                                  id: ScreenIds.sideDrawerToggle,
                                }
                              }
                            }
                          }
                        },
                      ]
                    },
                  },
                  {
                    stack: {
                      children: [
                        {
                          component: {
                            name: ScreenNames.sharePlace,
                            options: {
                              bottomTab: {
                                text: 'Share Place',
                                icon: sources[1],
                                selectedIconColor: '#3f6a81',
                                iconColor: '#8299b4',
                              },
                              topBar: {
                                leftButtons: {
                                  icon: sources[2],
                                  id: ScreenIds.sideDrawerToggle,
                                }
                              }
                            }
                          }
                        },
                      ]
                    },
                  },
                ],
              },
            },
          },
        },
      });
    })
};

export { mainPage };