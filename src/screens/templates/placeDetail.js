import { ScreenNames } from '@utils/enums';

const placeDetailsTemplate = place => ({
    component: {
        name: ScreenNames.placeDetail,
        passProps: {
            selectedPlace: place,
        },
        options: {
            topBar: {
                title: {
                    text: place.name,
                },
            },
        },
    },
});

export { placeDetailsTemplate };