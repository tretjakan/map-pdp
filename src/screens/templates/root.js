import { ScreenNames, ScreenIds } from '@utils/enums';

export const rootTemplate = {
  root: {
    stack: {
      id: ScreenIds.main,
      children: [
        {
          component: {
            name: ScreenNames.authScreen,
            options: {
              topBar: {
                title: {
                  text: 'Map App'
                },
              },
            },
          },
        },
      ],
    },
  }
};