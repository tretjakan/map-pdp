import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Animated } from 'react-native';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import { bindActionCreators } from 'redux';
import { PlaceList } from '@components';
import { getPlaces } from '@store/actions';
import { placeDetailsTemplate } from '@templates';
import { ScreenIds } from '@utils/enums';

class FindPlaceScreenComponent extends React.Component {
  state = {
    isSideDrawerVisible: true,
    isLoading: false,
    removeAnimation: new Animated.Value(1),
    placesAnim: new Animated.Value(0),
  }

  componentDidMount = () => {
    const { onGetPlaces } = this.props;
    this.navigationEventListener = Navigation.events().bindComponent(this);

    onGetPlaces();
  }

  componentWillUnmount = () => {
    if (this.navigationEventListener) {
      this.navigationEventListener.remove(this.onUpdateDrawer);
    }
  }

  componentDidAppear = () => {
    const { onGetPlaces } = this.props;
    onGetPlaces();
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === ScreenIds.sideDrawerToggle) {
      this.onUpdateDrawer();
    }
  }

  onUpdateDrawer = () => {
    const { isSideDrawerVisible } = this.state;
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        left: {
          visible: isSideDrawerVisible,
        }
      }
    });
  }

  onItemSelectedHandler = (key) => {
    const { places } = this.props;
    const currentPlace = places.find(place => place.key === key);
    Navigation.push(this.props.componentId, placeDetailsTemplate(currentPlace))
  }

  placesLoadedHandler = () => {
    const { placesAnim } = this.state;
    Animated.timing(placesAnim, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true
    }).start();
  };

  onFindClick = () => {
    // this.setState({ isLoading: true });
    const { removeAnimation } = this.state;
    Animated.timing(removeAnimation, {
      toValue: 0,
      duration: 500,
      useNativeDriver: true,
    }).start(() => {
      this.setState({
        isLoading: true
      });
      this.placesLoadedHandler();
    });
  }

  render() {
    const { places } = this.props;
    const { isLoading, removeAnimation, placesAnim } = this.state;
    return (
      <View style={isLoading ? null : styles.button}>
        {isLoading ? (
          <Animated.View
            style={{
              opacity: placesAnim,
            }}
          >
            <PlaceList
              places={places}
              onItemSelected={(key) => this.onItemSelectedHandler(key)}
            />
          </Animated.View>
        ) : (
            <Animated.View
              style={{
                opacity: removeAnimation,
                transform: [
                  {
                    scale: removeAnimation.interpolate({
                      inputRange: [0, 1],
                      outputRange: [12, 1],
                    }),
                  },
                ]
              }}
            >
              <TouchableOpacity onPress={this.onFindClick}>
                <View style={styles.search}>
                  <Text style={styles.text}>Find Places</Text>
                </View>
              </TouchableOpacity>
            </Animated.View>
          )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  search: {
    borderColor: '#182f38',
    borderWidth: 2,
    borderRadius: 50,
    padding: 20,
    backgroundColor: '#3f6a81',
  },
  text: {
    color: '#e4e7eb',
    fontWeight: 'bold',
    fontSize: 26,
  },
})

const mapStateToProps = state => ({
  places: state.places.places,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  onGetPlaces: getPlaces,
}, dispatch);

const FindPlaceScreen = connect(mapStateToProps, mapDispatchToProps)(FindPlaceScreenComponent);

export { FindPlaceScreen };