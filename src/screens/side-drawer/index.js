import React from 'react';
import {
  View, Text, Dimensions, StyleSheet, TouchableOpacity, Platform,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logout } from '@store/actions';

class SideDrawerComponent extends React.Component {

  onGoToRoot = () => {
    const { onLogout } = this.props;
    onLogout();
  }

  render() {
    return (
      <View style={[styles.container, { width: Dimensions.get('window').width * 0.8 }]}>
        <TouchableOpacity onPress={() => this.onGoToRoot()}>
          <View style={styles.drawerItem}>
            <Icon
              style={styles.drawerIcon}
              name={Platform.OS === 'android' ? 'md-log-out' : 'ios-log-out'}
              size={30}
              color="#182f38"
            />
            <Text>Sign Out</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 22,
    backgroundColor: 'white',
    flex: 1,
    paddingTop: 50,
  },
  drawerItem: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: '#e4e7eb',
  },
  drawerIcon: {
    marginRight: 10,
  },
})

const mapDispatchToProps = dispatch => bindActionCreators({
  onLogout: logout,
}, dispatch)

const SideDrawerScreen = connect(null, mapDispatchToProps)(SideDrawerComponent);

export { SideDrawerScreen };