import React from 'react';
import { View, StyleSheet, ScrollView, Button, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Navigation } from 'react-native-navigation';
import { Title } from '@components/ui/title';
import { Typography } from '@components/ui/text';
import { PickImage } from '@components/pick-image';
import { PickLocation } from '@components/pick-location';
import { PlaceInput } from '@components';
import { validate } from '@utils/validation';
import { startAddPlace, addPlace } from '@store/actions';
import { ScreenIds } from '@utils/enums';

class SharePlaceScreenComponent extends React.Component {
  componentDidMount = () => {
    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentWillMount() {
    this.onReset();
  }


  componentWillUnmount = () => {
    if (this.navigationEventListener) {
      this.navigationEventListener.remove();
    }
  }

  componentDidUpdate = () => {
    const { isPlaceAdded, onEndAddPlace } = this.props;
    if (isPlaceAdded) {
      Navigation.mergeOptions(this.props.componentId, {
        bottomTabs: {
          currentTabIndex: 0
        }
      });
      onEndAddPlace();
    }
  }

  componentDidAppear() {
    const { onEndAddPlace } = this.props;
    onEndAddPlace();
  }

  onReset = () => {
    this.setState({
      isSideDrawerVisible: true,
      controls: {
        placeName: {
          value: "",
          valid: false,
          touched: false,
          validationRules: {
            notEmpty: true
          }
        },
        location: {
          value: null,
          valid: false
        },
        image: {
          value: null,
          valid: false,
        },
      },
    })
  }

  navigationButtonPressed({ buttonId }) {
    const { isSideDrawerVisible } = this.state;
    if (buttonId === ScreenIds.sideDrawerToggle) {
      Navigation.mergeOptions(this.props.componentId, {
        sideMenu: {
          left: {
            visible: isSideDrawerVisible,
          }
        }
      });
    }
  }

  onChange = (val) => {
    this.setState(prevState => {
      return {
        controls: {
          ...prevState.controls,
          placeName: {
            ...prevState.controls.placeName,
            value: val,
            valid: validate(val, prevState.controls.placeName.validationRules),
            touched: true
          },
        }
      };
    });
  };

  onPlaceAdded = () => {
    const { onAddPlace } = this.props;
    const { controls: { placeName, location, image } } = this.state;

    onAddPlace(placeName.value, location.value, image.value);
    this.onReset();
    this.imagePicker.onReset();
    this.locationPicker.onReset();
  }

  onPickLocation = (location) => {
    this.setState((prevState) => {
      return {
        controls: {
          ...prevState.controls,
          location: {
            value: location,
            valid: true,
          }
        },
      };
    });
  }

  onPickImage = (image) => {
    this.setState((prevState) => {
      return {
        controls: {
          ...prevState.controls,
          image: {
            value: image,
            valid: true,
          },
        },
      };
    });
  }

  render() {
    const { controls } = this.state;
    const { isLoading } = this.props;
    return (
      <ScrollView>
        <View style={styles.container}>
          <Typography>
            <Title>Share a Place with us!</Title>
          </Typography>
          <PickImage
            onImagePicked={this.onPickImage}
            ref={ref => this.imagePicker = ref}
          />
          <PickLocation
            onLocationPick={this.onPickLocation}
            ref={ref => this.locationPicker = ref}
          />
          <PlaceInput
            placeData={controls.placeName}
            onChangeText={this.onChange}
          />
          <View style={styles.button}>
            {isLoading ? <ActivityIndicator /> : (
              <Button
                title="Share the Place!"
                onPress={this.onPlaceAdded}
                disabled={
                  !controls.placeName.valid ||
                  !controls.location.valid ||
                  !controls.image.valid
                }
              />
            )}
          </View>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#819cab',
  },
  button: {
    margin: 8,
  },
});

const mapStateToProps = state => ({
  isLoading: state.ui.isLoading,
  isPlaceAdded: state.places.isPlaceAdded,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  onAddPlace: addPlace,
  onEndAddPlace: startAddPlace,
}, dispatch);

const SharePlaceScreen = connect(mapStateToProps, mapDispatchToProps)(SharePlaceScreenComponent);

export { SharePlaceScreen };