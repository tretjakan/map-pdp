import React from 'react';
import { connect } from 'react-redux';
import {
  View, ImageBackground, StyleSheet,
  Dimensions, KeyboardAvoidingView, Keyboard,
  TouchableWithoutFeedback, ActivityIndicator,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { DefaultInput } from '@components/ui/default-input';
import { Title } from '@components/ui/title';
import { Button } from '@components/ui/button';
import BackgroundImage from '../../assests/background.jpg';
import { validate } from '@utils/validation';
import { tryAuth, authAutoSingIn } from '@store/actions';

class AuthScreenComponent extends React.Component {
  static types = {
    portrait: 1,
    landscape: 2,
  }

  static authModes = {
    login: 1,
    sighUp: 2,
  }

  static keys = {
    password: 'password',
    email: 'email',
    confirmPassword: 'confirmPassword',
  }

  constructor(props) {
    super(props);
    this.state = {
      authMode: AuthScreenComponent.authModes.login,
      viewMode: null, controls: {
        [AuthScreenComponent.keys.email]: {
          value: '',
          isValid: false,
          validationRules: {
            isEmail: true,
          },
          isTouched: false,
        },
        [AuthScreenComponent.keys.password]: {
          value: '',
          isValid: false,
          validationRules: {
            minLength: 6,
          },
          isTouched: false,
        },
        [AuthScreenComponent.keys.confirmPassword]: {
          value: '',
          isValid: false,
          validationRules: {
            equalTo: 'password',
          },
          isTouched: false,
        },
      }
    }
  }

  componentDidMount = () => {
    const { onAutoAuth } = this.props;
    Dimensions.addEventListener('change', this.updateStyles);

    onAutoAuth();
  }

  componentWillUnmount = () => {
    Dimensions.removeEventListener('change', this.updateStyles);
  }

  onSwitchAuth = () => {
    this.setState((prevState) => {
      return {
        authMode: prevState.authMode === AuthScreenComponent.authModes.login
          ? AuthScreenComponent.authModes.sighUp : AuthScreenComponent.authModes.login,
      }
    })
  }

  updateStyles = (dims) => {
    this.setState({
      viewMode: dims.window.height > 500
        ? AuthScreen.types.portrait : AuthScreen.types.landscape,
    })
  }

  state = {
    viewMode: Dimensions.get('window').height > 500
      ? AuthScreen.types.portrait : AuthScreen.types.landscape,
  }

  pushViewPostScreen = () => {
    const { onAuth } = this.props;
    const { controls: { email, password }, authMode } = this.state;
    onAuth({
      email: email.value,
      password: password.value,
    }, authMode);
  }

  onChange = (key, value) => {
    const { controls } = this.state;
    let connectedValue = {};
    if (controls[key].validationRules.equalTo) {
      const equalControl = controls[key].validationRules.equalTo;
      const equalValue = controls[equalControl].value;
      connectedValue = {
        ...connectedValue,
        equalTo: equalValue,
      };
    }
    if (key === AuthScreenComponent.keys.password) {
      connectedValue = {
        ...connectedValue,
        equalTo: value,
      };
    }
    this.setState((prevState) => {
      return {
        controls: {
          ...prevState.controls,
          [AuthScreenComponent.keys.confirmPassword]: {
            ...prevState.controls.confirmPassword,
            valid: key === AuthScreenComponent.keys.password ? validate(prevState.controls.confirmPassword.value, prevState.controls.confirmPassword.validationRules, connectedValue) : prevState.controls.confirmPassword.valid
          },
          [key]: {
            ...prevState.controls[key],
            value,
            isValid: validate(value, prevState.controls[key].validationRules, connectedValue),
            isTouched: true,
          },
        },
      };
    })
  }

  render() {
    const { isLoading } = this.props;
    const { viewMode, controls, authMode } = this.state;
    return (
      <ImageBackground source={BackgroundImage} style={styles.backgroundImage}>
        <KeyboardAvoidingView
          style={styles.container}
          behavior="padding"
        >
          {viewMode === AuthScreen.types.portrait && <Title style={styles.title}>Log In</Title>}
          <Button onPress={this.onSwitchAuth}>
            Switch to
            {'  '}
            {authMode === AuthScreenComponent.authModes.login ? 'Sign Up' : 'Login'}
          </Button>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.inputContainer}>
              <DefaultInput
                placeholder="Your email"
                value={controls.email.value}
                onChangeText={(value) => this.onChange(AuthScreenComponent.keys.email, value)}
                isValid={controls.email.isValid}
                isTouched={controls.email.isTouched}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="email-address"
              />
              <View style={viewMode === AuthScreen.types.portrait || authMode === AuthScreenComponent.authModes.login
                ? styles.portraitPasswordContainer : styles.landscapePasswordContainer}>
                <View style={viewMode === AuthScreen.types.portrait || authMode === AuthScreenComponent.authModes.login
                  ? styles.portraitPasswordWrapper : styles.landscapePasswordWrapper}>
                  <DefaultInput
                    placeholder="Password"
                    value={controls.password.value}
                    onChangeText={(value) => this.onChange(AuthScreenComponent.keys.password, value)}
                    isValid={controls.password.isValid}
                    isTouched={controls.password.isTouched}
                    secureTextEntry
                  />
                </View>
                {authMode === AuthScreenComponent.authModes.sighUp && (
                  <View style={viewMode === AuthScreen.types.portrait
                    ? styles.portraitPasswordWrapper : styles.landscapePasswordWrapper}>
                    <DefaultInput
                      placeholder="Confirm Password"
                      value={controls.confirmPassword.value}
                      onChangeText={(value) => this.onChange(AuthScreenComponent.keys.confirmPassword, value)}
                      isValid={controls.confirmPassword.isValid}
                      isTouched={controls.confirmPassword.isTouched}
                      secureTextEntry
                    />
                  </View>
                )}
              </View>
            </View>
          </TouchableWithoutFeedback>
          {isLoading ? <ActivityIndicator /> : (<Button
            onPress={this.pushViewPostScreen}
            isDisabled={
              !controls.confirmPassword.isValid && authMode === AuthScreenComponent.authModes.signUp
              || !controls.password.isValid
              || !controls.email.isValid
            }
          >
            Submit
          </Button>)}
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    width: '80%',
  },
  backgroundImage: {
    width: '100%',
    flex: 1,
  },
  landscapePasswordContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  portraitPasswordContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  landscapePasswordWrapper: {
    width: '45%',
  },
  portraitPasswordWrapper: {
    width: '100%',
  },
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  onAuth: tryAuth,
  onAutoAuth: authAutoSingIn,
}, dispatch);

const mapStateToProps = state => ({
  isLoading: state.ui.isLoading,
});

const AuthScreen = connect(mapStateToProps, mapDispatchToProps)(AuthScreenComponent);

export { AuthScreen };
