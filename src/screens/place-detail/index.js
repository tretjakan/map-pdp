import React from "react";
import {
  View,
  Image,
  Text,
  Platform,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from "react-native-vector-icons/Ionicons";
import { Navigation } from 'react-native-navigation';
import MapView from "react-native-maps";
import { deletePlace } from '@store/actions';
import { ViewModes, ScreenIds } from '@utils/enums';

class PlaceDetailComponent extends React.Component {
  state = {
    viewMode: ViewModes.portrait,
  };

  constructor(props) {
    super(props);
    Dimensions.addEventListener("change", this.updateStyles);
  }

  componentWillUnmount() {
    Dimensions.removeEventListener("change", this.updateStyles);
  }

  updateStyles = (dims) => {
    this.setState({
      viewMode: dims.window.height > 500 ? ViewModes.portrait : ViewModes.landscape,
    });
  };

  onPlaceDeleteHandler = () => {
    const { onDeletePlace, selectedPlace } = this.props;
    onDeletePlace(selectedPlace.key);
    Navigation.pop(ScreenIds.placeDetail);
  }

  render() {
    const { selectedPlace } = this.props;
    const { viewMode } = this.state;
    return (
      <View style={[
        styles.container,
        viewMode === ViewModes.portrait
          ? styles.portraitContainer
          : styles.landscapeContainer
      ]}
      >
        <View style={styles.placeDetailContainer}>
          <View style={styles.subContainer}>
            <Image source={selectedPlace.image} style={styles.placeImage} />
          </View>
          <View style={styles.subContainer}>
            <MapView
              initialRegion={{
                ...selectedPlace.location,
                latitudeDelta: 0.0122,
                longitudeDelta:
                  Dimensions.get("window").width /
                  Dimensions.get("window").height *
                  0.0122
              }}
              style={styles.map}
            >
              <MapView.Marker coordinate={selectedPlace.location} />
            </MapView>
          </View>
          <View style={styles.subContainer}>
            <View>
              <Text style={styles.placeName}>{selectedPlace.name}</Text>
            </View>
            <View>
              <TouchableOpacity onPress={this.onPlaceDeleteHandler}>
                <View style={styles.deleteButton}>
                  <Icon
                    size={30}
                    name={Platform.OS === 'android' ? 'md-trash' : 'ios-trash'}
                    color="red"
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    margin: 22,
    flex: 1,
  },
  placeImage: {
    width: "100%",
    height: 200
  },
  placeName: {
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 28
  },
  deleteButton: {
    alignItems: "center"
  },
  portraitContainer: {
    flexDirection: "column"
  },
  landscapeContainer: {
    flexDirection: "row"
  },
  subContainer: {
    flex: 1
  },
  placeDetailContainer: {
    flex: 2
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
});

const mapDispatchToProps = dispatch => bindActionCreators({
  onDeletePlace: deletePlace,
}, dispatch);

const PlaceDetailScreen = connect(null, mapDispatchToProps)(PlaceDetailComponent)

export { PlaceDetailScreen };