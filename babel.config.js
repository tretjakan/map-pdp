module.exports = function(api) {
  api.cache(true);
  const presets = ["module:metro-react-native-babel-preset"];
  const plugins = [
    ["module-resolver", {
      "root": ["./"],
      "alias": {
        "@components": "./src/components",
        "@store": "./src/store",
        "@utils": "./src/utils",
        "@common": "./assets",
        "@templates": "./src/screens/templates",
        "@screens": "./src/screens",
      }
    }]
  ];
  return {
    presets,
    plugins,
  };
};
