import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import {
  AuthScreen, SharePlaceScreen, FindPlaceScreen,
  PlaceDetailScreen, SideDrawerScreen,
} from '@screens';
import { rootTemplate } from '@templates';
import { ScreenNames } from '@utils/enums';
import { configureStore } from '@store/store';

const store = configureStore();

// Register Screens
Navigation.registerComponentWithRedux(ScreenNames.authScreen, () => AuthScreen, Provider, store);
Navigation.registerComponentWithRedux(ScreenNames.sharePlace, () => SharePlaceScreen, Provider, store);
Navigation.registerComponentWithRedux(ScreenNames.findPlace, () => FindPlaceScreen, Provider, store);
Navigation.registerComponentWithRedux(ScreenNames.placeDetail, () => PlaceDetailScreen, Provider, store);
Navigation.registerComponentWithRedux(ScreenNames.sideDrawer, () => SideDrawerScreen, Provider, store);

// Start a App
Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot(rootTemplate);
});
