const functions = require('firebase-functions');
const cors = require('cors')({ origin: true });
const admin = require('firebase-admin');
const fs = require('fs');
const uuid = require('uuid-v4');

// Imports the Google Cloud client library
const { Storage } = require('@google-cloud/storage');

// Your Google Cloud Platform project ID
const projectId = 'map-app-1554061408740';

// Creates a client
const gcs = new Storage({
  projectId,
  keyFilename: 'gsc.json',
});

admin.initializeApp({
  credential: admin.credential.cert(require('./gsc.json')),
});

// const { Storage } = require('@google-cloud/storage');

// const gcs = new Storage({
//     projectId: 'map-app-1554061408740',
//     keyFilename: 'gsc.json'
// });

exports.storeImage = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    if (!request.headers.authorization || !request.headers.authorization.startsWith('Bearer ')) {
      console.log('No token present!');
      response.status(403).json({ error: 'Unathorized' });
      return;
    }
    let token = request.headers.authorization.split('Bearer ')[1];
    admin.auth().verifyIdToken(token)
      .then((decodedToken) => {
        const body = JSON.parse(request.body);
        fs.writeFileSync('/tmp/uploaded-image.jpg', body.image, 'base64', (error) => {
          console.log(err);
          return response.status(500).json({ error });
        });
        const id = uuid();
        const bucket = gcs.bucket('map-app-1554061408740.appspot.com');
        bucket.upload('/tmp/uploaded-image.jpg', {
          uploadType: 'media',
          destination: `/places/${id}.jpg`,
          metadata: {
            metadata: {
              contentType: 'image/jpeg',
              firebaseStorageDownloadTokens: id,
            },
          },
        }, (error, file) => {
          if (!error) {
            response.status(201).json({
              imageUrl: `https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/${encodeURIComponent(file.name)}?alt=media&token=${id}`,
              imagePath: `/places/${id}.jpg`,
            });
          } else {
            console.log(error);
            response.status(500).json({ error });
          }
        });
      })
      .catch((error) => {
        console.log('Token is invalid');
        response.status(403).json({ error: 'Unauthorize' });
      });
  });
});

exports.deleteImage = functions.database.ref('/places/{placeId}').onDelete(event => {
  const placeData = event.data.previous.val();
  const imagePath = placeData.imagePath;

  const bucket = gcs.bucket('map-app-1554061408740.appspot.com');
  return bucket.file(imagePath).delete();
});
